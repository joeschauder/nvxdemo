﻿using System;
using System.Collections.Generic;
using System.Text;
using Crestron.SimplSharpPro.DM.Streaming;
using System.ComponentModel;

namespace NVX_VC_demo
{
    class Room
    {
        private string _name;
        ushort _sourceCount;
        ushort _destinationCount;


        public static List<Room> roomlist = new List<Room>();
        public static List<DmNvx350> nvxlist = new List<DmNvx350>();
        public static List<string> sourcelist = new List<string>();
        public static List<string> destinationlist = new List<string>();

        public static event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public ushort returnSourceCount()
        {
            return (ushort)sourcelist.Count;
        }

        public ushort returnDestinationCount()
        {
            return (ushort)destinationlist.Count;
        }

        public ushort getSourceCount()
        {
            foreach (var nvx in nvxlist)
            {
                
            }

            return 1;
        }



    }
}
