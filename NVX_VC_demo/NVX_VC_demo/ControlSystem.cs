using System;
using System.IO;
using Crestron.SimplSharp;                              
using Crestron.SimplSharpPro;                           
using Crestron.SimplSharpPro.CrestronThread;                   
using Crestron.SimplSharp.WebScripting;                

namespace NVX_VC_demo
{
    public class Server_Handler : IHttpCwsHandler
    {
        private ControlSystem _cs;
        public Server_Handler(ControlSystem cs)
        {
            _cs = cs;
        }

        void IHttpCwsHandler.ProcessRequest(HttpCwsContext context)
        {
            try
            {
                // handle requests
                if (context.Request.RouteData != null)
                {
                    switch (context.Request.RouteData.Route.Name.ToUpper())
                    {
                        case "ROOMNAME":
                            context.Response.StatusCode = 200;
                            context.Response.Write(InitialParametersClass.RoomName, true);
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                ErrorLog.Error("CWS Handler: {0}", e.Message);
            }

        }
    }

    public class ControlSystem : CrestronControlSystem
    {

        private string myFolder { get { return Crestron.SimplSharp.CrestronIO.Directory.GetApplicationRootDirectory() + "/User/"; } }
        private HttpCwsServer myServer;

        public ControlSystem()
            : base()
        {
            try
            {
                Thread.MaxNumberOfUserThreads = 20;

                //Subscribe to the controller events (System, Program, and Ethernet)
                CrestronEnvironment.SystemEventHandler += new SystemEventHandler(ControlSystem_ControllerSystemEventHandler);
                CrestronEnvironment.ProgramStatusEventHandler += new ProgramStatusEventHandler(ControlSystem_ControllerProgramEventHandler);
                CrestronEnvironment.EthernetEventHandler += new EthernetEventHandler(ControlSystem_ControllerEthernetEventHandler);

                ErrorLog.Notice("*** : Constructor");
                ErrorLog.Notice("*** : Current Time is {0}", DateTime.Now);
                myServer = new HttpCwsServer("api");
                myServer.Routes.Add(new HttpCwsRoute("roomname") { Name = "ROOMNAME" });
                myServer.HttpRequestHandler = new Server_Handler(this);
                myServer.Register();
            }
            catch (Exception e)
            {
                ErrorLog.Error("Error in the constructor: {0}", e.Message);
            }
        }

        public override void InitializeSystem()
        {
            try
            {
                ErrorLog.Notice("*** : InitializeSystem");
                using (FileStream myStream = File.Create(myFolder + "test.txt"))
                {
                    byte[] whatToWrite = System.Text.Encoding.ASCII.GetBytes(String.Format("Program Running Name: {0}", InitialParametersClass.RoomName));
                    myStream.Write(whatToWrite,0,whatToWrite.Length);
                    myStream.Flush();
                }


            }
            catch (Exception e)
            {
                ErrorLog.Error("Error in InitializeSystem: {0}", e.Message);
            }

        }

        void ControlSystem_ControllerEthernetEventHandler(EthernetEventArgs ethernetEventArgs)
        {
            switch (ethernetEventArgs.EthernetEventType)
            {
                case (eEthernetEventType.LinkDown):

                    if (ethernetEventArgs.EthernetAdapter == EthernetAdapterType.EthernetLANAdapter)
                    {
                    }
                    break;
                case (eEthernetEventType.LinkUp):
                    if (ethernetEventArgs.EthernetAdapter == EthernetAdapterType.EthernetLANAdapter)
                    {

                    }
                    break;
            }
        }

        void ControlSystem_ControllerProgramEventHandler(eProgramStatusEventType programStatusEventType)
        {
            switch (programStatusEventType)
            {
                case (eProgramStatusEventType.Paused):
                    ErrorLog.Notice("*** : Pause");
                    break;
                case (eProgramStatusEventType.Resumed):
                    ErrorLog.Notice("*** : Resumed");
                    break;
                case (eProgramStatusEventType.Stopping):
                    ErrorLog.Notice("*** : Stopping");
                    break;
            }

        }

        void ControlSystem_ControllerSystemEventHandler(eSystemEventType systemEventType)
        {
            switch (systemEventType)
            {
                case (eSystemEventType.DiskInserted):
                    ErrorLog.Notice("*** : Disk Inserted");
                    break;
                case (eSystemEventType.DiskRemoved):
                    ErrorLog.Notice("*** : Disk Removed");
                    break;
                case (eSystemEventType.Rebooting):
                    ErrorLog.Notice("*** : Rebooting");
                    break;
            }

        }
    }
}
