﻿using System;
using Crestron.SimplSharpPro.UI;
using Crestron.SimplSharpPro.DeviceSupport;
using System.ComponentModel;


namespace NVX_VC_demo
{
    class tpPressHandler
    {
        private string _destinationSelected;
        private string  _roomSelected;
        private string _sourceSelected;



        public static event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public string sourceSelected
        {
            get
            {
                return _sourceSelected;
            }

            set
            {
                _sourceSelected = value;
                RaisePropertyChanged("sourceSelected");
            }
        }

        public string destinationSelected
        {
            get
            {
                return _destinationSelected;
            }

            set
            {
                _destinationSelected = value;
                RaisePropertyChanged("destinationSelected");
            }
        }

        public string roomSelected
        {
            get
            {
                return _roomSelected;
            }

            set
            {
                _roomSelected = value;
                RaisePropertyChanged("roomSelected");
            }

        }

        public static void AddActionstoTPJoins(Tsw760 tp)
        {
            (tp.BooleanOutput[1].UserObject as DigitalJoinUserObject).DigitalPressAction = new Action<BasicTriList>((touchpanel) => emptymethod(tp));
        }

        private static void emptymethod(Tsw760 tp)
        {
            throw new NotImplementedException();
        }
    }
}
