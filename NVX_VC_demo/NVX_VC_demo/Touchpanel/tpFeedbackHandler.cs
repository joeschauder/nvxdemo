﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace NVX_VC_demo
{
    class tpFeedbackHandler
    {
        public static void connectToSourceSelectUpdates(ushort touchpanel, ushort room)
        {
            tpPressHandler.PropertyChanged += new PropertyChangedEventHandler(RoomPropertyChanged);
        }

        public static void RoomPropertyChanged(object o, PropertyChangedEventArgs args)
        {
            switch (args.PropertyName)
            {
                case "SourceSelected":
                    {
                        updateSourceSelectFeedback();
                        break;
                    }

                case "DestinationSelected":
                    {

                        break;
                    }
            }
        }

        

        public static void updateSourceSelectFeedback()
        {
            sigGroups.sourceselectSigGroup.BoolValue = false;
            //Hardware.officeTSW.BooleanInput[(uint)(9 + TPPressActions.sourceSelected)].BoolValue = true;
        }
    }
}
