﻿using Crestron.SimplSharpPro.UI;
using Crestron.SimplSharpPro;


namespace NVX_VC_demo
{
    class sigGroups
    {
        public static SigGroup sourceselectSigGroup;

        public static void addSigGroups(Tsw760 mytsw)
        {
            //Source Select
            sourceselectSigGroup.Add(mytsw.BooleanInput[10]);
            sourceselectSigGroup.Add(mytsw.BooleanInput[11]);
            sourceselectSigGroup.Add(mytsw.BooleanInput[12]);
            sourceselectSigGroup.Add(mytsw.BooleanInput[13]);
            sourceselectSigGroup.Add(mytsw.BooleanInput[14]);
            sourceselectSigGroup.Add(mytsw.BooleanInput[15]);
        }


        public static void sourceSelectFeedback()
        {
            sourceselectSigGroup.BoolValue = false;
        }
    }
}
